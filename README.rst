==========
!ARCHIVED!
==========

This project has been archived, along with all other POP and Idem-based projects.

* For more details: `Salt Project Blog - POP and Idem Projects Will Soon be Archived <https://saltproject.io/blog/2025-01-24-idem-pop-projects-archived/>`__

===============
IDEM_VIRTUALBOX
===============

# VirtualBox Provider for Idem

============
INSTALLATION
============

The virtualbox idem provider can be installed via pip:
`pip install idem-virtualbox`

============================
INSTALLATION FOR DEVELOPMENT
============================

1. Clone the `idem_provider_virtualbox` repository and install with pip:
`pip install -r requirements.txt`
2. Run `pip install -e <path to provider>` from your project's root directory

You are now fully set up to begin developing additional functionality for this provider.

=========
EXECUTION
=========

After installation the Azure Resource Manager Idem Provider execution and state modules will be accessible to the hub.

The following example uses a virtualbox state module to ensure the existence of vm::

    VM exists:
      vbox.present:
        - name: instance_name


Use the command line to run virtualbox specific execution modules::

    idem exec vbox.list

