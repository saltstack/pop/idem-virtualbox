import logging
import pytest
import random
import string
import tempfile
import virtualbox

log = logging.getLogger(__name__)

if not virtualbox.import_vboxapi():
    pytest.skip("vbox-sdk is required to run these tests", allow_module_level=True)
    virtualbox.Manager()


@pytest.fixture(scope="session")
def vbox():
    """
    Create a temporary folder for the test machines
    """
    tmp = tempfile.TemporaryDirectory(prefix="VirtualBoxVMs_", suffix="_Test")
    log.debug(f"Created temporary directory at {tmp.name}")

    vbox = virtualbox.VirtualBox()
    log.debug("saving default machine folder")
    default_machine_folder = vbox.system_properties.default_machine_folder

    log.debug("setting new default machine folder")
    vbox.system_properties.default_machine_folder = tmp.name

    yield vbox
    log.debug("restoring default machine folder")
    vbox.system_properties.default_machine_folder = default_machine_folder
    tmp.cleanup()


@pytest.fixture(scope="session")
def image(vbox) -> str:
    new_machine = vbox.create_machine(
        settings_file="", name="test_blank_machine", os_type_id="", groups=[], flags=""
    )
    vbox.register_machine(new_machine)

    yield new_machine.name
    new_machine.unregister(cleanup_mode=virtualbox.lib.CleanupMode(2))


@pytest.fixture
def hub(hub):
    hub.log.debug("Initializing hub")
    hub.pop.sub.add("idem.idem")
    yield hub


@pytest.fixture(scope="module")
def instance_name():
    yield "test_idem_cloud_" + "".join(
        random.choice(string.ascii_lowercase + string.digits) for _ in range(20)
    )
