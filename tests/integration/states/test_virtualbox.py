def test_present(hub, ctx, instance_name, image):
    ret = hub.states.vbox.present(ctx, instance_name, clone_from=image)
    assert ret == {}


def test_absent(hub, ctx, instance_name):
    ret = hub.states.vbox.absent(ctx, instance_name)
    assert ret == {}
